#!/usr/bin/env ruby
require 'fileutils'
require 'forwardable'
require 'tempfile'
require 'socket'

class Hosts
  extend Forwardable

  DEFAULTS = {
    :path => '/etc/hosts',
  }

  attr_reader :hosts
  attr_reader :file

  def_delegators :@hosts, :[], :[]=, :delete, :clear

  def initialize(options = {})
    @options = DEFAULTS.merge(options)
    @hosts = parse
  end

  def path
    @options.fetch(:path)
  end

  def open
    File.open(self.path) do |hosts_file|
      hosts_file.flock(File::LOCK_EX)

      yield

      Tempfile.open(File.basename(__FILE__)) do |f|
        f.puts self.to_s
        f.flush
        FileUtils.cp(f.path, self.path)
      end
    end

    nil
  end

  def to_s
    localhost = @hosts.delete('127.0.0.1')
    hosts = @hosts.to_a.sort
    hosts.unshift(['127.0.0.1', localhost]) if localhost
    hosts.map {|k, v| k + "\t" + v.uniq.join(' ') }.join("\n")
  end

  private

  def parse
    hosts = {}

    File.open(self.path) do |f|
      f.each_line do |l|
        next if l =~ /\A\s*#/
        l.sub!(/#.*\Z/, '')
        addr, names = l.strip.split(/\s+/, 2)

        if (addr || '').strip.empty? or (names || '').strip.empty?
          next
        end

        names = names.strip.split(/\s+/)
        hosts[addr] = names
      end
    end

    return hosts
  end
end

class Serf
  DEFAULTS = {
    :path => 'serf'
  }

  def initialize(options = {})
    @options = DEFAULTS.merge(options)
  end

  def members
    lines = []

    `#{@options[:path]} members`.each_line do |l|
      lines << l.strip.split(/\s+/)
    end

    return lines
  end

  def event
    ENV['SERF_EVENT']
  end
end

def gethostname
  hostname = (`hostname` || '').strip

  if hostname.empty?
    Socket.gethostname
  else
    hostname
  end
end

def update_hosts(serf, hostname, hosts)
  hosts.open do
    localhost = hosts.delete('127.0.0.1') || []
    localhost << hostname
    hosts.clear
    hosts['127.0.0.1'] = localhost

    serf.members.each do |l|
      name, addr = l.values_at(0, 1)
      next if name == hostname
      addr = addr.split(':', 2).first
      hosts[addr] ||= []
      hosts[addr] << [name]
    end
  end
end

if $0 == __FILE__
  serf  = ENV['SERF_PATH'] ? Serf.new(:path => ENV['SERF_PATH']) : Serf.new
  hosts = ENV['HOSTS_PATH'] ? Hosts.new(:path => ENV['HOSTS_PATH']) : Hosts.new
  hostname = gethostname

  if serf.event =~ /\Amember-/
    update_hosts(serf, hostname, hosts)
  end
end
