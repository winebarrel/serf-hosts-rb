# serf-hosts.rb

It is event handler script of [Serf](http://www.serfdom.io/) to update the hosts file.

[![Build Status](https://drone.io/bitbucket.org/winebarrel/serf-hosts-rb/status.png)](https://drone.io/bitbucket.org/winebarrel/serf-hosts-rb/latest)

## Usage

**node1**:

    $ serf agent -event-handler=serf-hosts.rb &

**node2**:

    $ serf agent -event-handler=serf-hosts.rb &
    $ serf join ZZZ.ZZZ.ZZZ.ZZZ & # join node1

Each hosts file is updated as follows:

**node1**:

```
127.0.0.1	localhost node1
XXX.XXX.XXX.XXX	node2
```

**node2**:

```
127.0.0.1       localhost node2
ZZZ.ZZZ.ZZZ.ZZZ	node1
```
