describe 'serf-hosts.rb' do
  it 'can update the hosts using Hosts class' do
    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |f|
      hosts = Hosts.new(:path => f.path)

      hosts.open do
        localhost = hosts['127.0.0.1']
        localhost << 'baz'
        hosts.clear
        hosts['127.0.0.1'] = localhost
        hosts['10.0.0.1'] = ['foo', 'bar']
        hosts['10.0.0.2'] = ['zoo', 'baz']
      end
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost baz
10.0.0.1	foo bar
10.0.0.2	zoo baz
    EOS
  end

  it 'can update the hosts without comments' do
    hosts = <<-EOS
127.0.0.1    localhost
# foo
192.168.1.1  foo
# bar
192.168.1.2  bar
# zooo
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |f|
      hosts = Hosts.new(:path => f.path)

      hosts.open do
        localhost = hosts['127.0.0.1']
        localhost << 'baz'
        hosts.clear
        hosts['127.0.0.1'] = localhost
        hosts['10.0.0.1'] = ['foo', 'bar']
        hosts['10.0.0.2'] = ['zoo', 'baz']
      end
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost baz
10.0.0.1	foo bar
10.0.0.2	zoo baz
    EOS
  end

  it 'can update the hosts using update_hosts method' do
    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |f|
      serf = double('serf')
      serf.should_receive(:members) { [
        %w(agent-one     172.20.20.10:7946    alive),
        %w(agent-two     172.20.20.11:7946    alive),
      ] }

      hostname = 'my-host'
      hosts = Hosts.new(:path => f.path)
      update_hosts(serf, hostname, hosts)
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost my-host
172.20.20.10	agent-one
172.20.20.11	agent-two
    EOS
  end

  it 'can update the hosts using update_hosts method (with localhost)' do
    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |f|
      serf = double('serf')
      serf.should_receive(:members) { [
        %w(agent-one     172.20.20.10:7946    alive),
        %w(agent-two     172.20.20.11:7946    alive),
      ] }

      hostname = 'agent-one'
      hosts = Hosts.new(:path => f.path)
      update_hosts(serf, hostname, hosts)
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost agent-one
172.20.20.11	agent-two
    EOS
  end

  it 'hostnames should be unique' do
    hosts = <<-EOS
127.0.0.1    localhost my-host
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |f|
      serf = double('serf')
      serf.should_receive(:members) { [
        %w(agent-one     172.20.20.10:7946    alive),
        %w(agent-two     172.20.20.11:7946    alive),
      ] }

      hostname = 'my-host'
      hosts = Hosts.new(:path => f.path)
      update_hosts(serf, hostname, hosts)
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost my-host
172.20.20.10	agent-one
172.20.20.11	agent-two
    EOS
  end

  it 'can update the hosts using Serf class' do
    serf = <<-EOS
echo 'agent-one     172.20.20.10:7946    alive'
echo 'agent-two     172.20.20.11:7946    alive'
    EOS

    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    result = tempfile(hosts) do |hosts_file|
      Dir.mktmpdir do |dir|
        serf_path = "#{dir}/serf"
        open(serf_path, 'w') {|f| f << serf }
        FileUtils.chmod(0755, serf_path)
        serf = Serf.new(:path => serf_path)

        hostname = 'my-host'
        hosts = Hosts.new(:path => hosts_file.path)
        update_hosts(serf, hostname, hosts)
      end
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost my-host
172.20.20.10	agent-one
172.20.20.11	agent-two
    EOS
  end

  it 'can update the hosts using serf-hosts.rb' do
    serf = <<-EOS
echo 'agent-one     172.20.20.10:7946    alive'
echo 'agent-two     172.20.20.11:7946    alive'
    EOS

    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    serf_hosts_rb = File.expand_path('../../serf-hosts.rb', __FILE__)

    result = tempfile(hosts) do |hosts_file|
      Dir.mktmpdir do |dir|
        serf_path = "#{dir}/serf"
        open(serf_path, 'w') {|f| f << serf }
        FileUtils.chmod(0755, serf_path)
        File.expand_path('../../serf-hosts.rb', __FILE__)

        env = {
          'SERF_EVENT' => 'member-join',
          'SERF_PATH'  => serf_path,
          'HOSTS_PATH' => hosts_file.path,
        }

        system(env, serf_hosts_rb)
      end
    end

    expect(result).to eq(<<-EOS)
127.0.0.1	localhost #{`hostname`.strip}
172.20.20.10	agent-one
172.20.20.11	agent-two
    EOS
  end

  it 'should not be updated in the non member-* event' do
    serf = <<-EOS
echo 'agent-one     172.20.20.10:7946    alive'
echo 'agent-two     172.20.20.11:7946    alive'
    EOS

    hosts = <<-EOS
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS

    serf_hosts_rb = File.expand_path('../../serf-hosts.rb', __FILE__)

    result = tempfile(hosts) do |hosts_file|
      Dir.mktmpdir do |dir|
        serf_path = "#{dir}/serf"
        open(serf_path, 'w') {|f| f << serf }
        FileUtils.chmod(0755, serf_path)
        File.expand_path('../../serf-hosts.rb', __FILE__)

        env = {
          'SERF_EVENT' => 'user',
          'SERF_PATH'  => serf_path,
          'HOSTS_PATH' => hosts_file.path,
         }

         system(env, serf_hosts_rb)
      end
    end

    expect(result).to eq(<<-EOS)
127.0.0.1    localhost
192.168.1.1  foo
192.168.1.2  bar
192.168.1.3  zoo
    EOS
  end

  it 'can get the hostname' do
    expect(gethostname).to eq(`hostname`.strip)
  end

  it 'can get Serf event' do
    serf = Serf.new
    ENV['SERF_EVENT'] = 'member-join'

    expect(serf.event).to eq('member-join')
  end
end
