$: << File.expand_path('../..', __FILE__)
require 'serf-hosts'
require 'fileutils'
require 'tempfile'
require 'tmpdir'

def tempfile(content = nil)
  basename = File.basename(__FILE__)
  retval = nil

  Tempfile.open(basename) do |f|
    if content
      f << content
      f.flush
      f.rewind
    end

    yield(f)

    retval = File.read(f.path)
  end

  return retval
end
